import { currentSlide } from './main.js'

export function addElement(parentId, elementTag, attr, html) {
    // Adds an element to the document
    var p = document.getElementById(parentId);
    var newElement = document.createElement(elementTag);
    newElement.setAttribute('id', attr.id);
    newElement.setAttribute('onclick', attr.onclick);
    newElement.setAttribute('class', attr.class);
    if (html)
        newElement.innerHTML = html;
    p.appendChild(newElement);
}

export function removeElement(elementId) {
    // Removes an element from the document
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}