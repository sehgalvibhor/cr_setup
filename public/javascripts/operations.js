import {updateInsert, updateRemove, updateReplace, undoQuill, redoQuill} from './quill.js'
import {readSentence, readSelection, read} from './tts.js'

export var insert = function(pos, slot, formatString) {
    return new Promise(function(resolve) {
        updateInsert(pos, slot, formatString).then(function() {
            resolve();
        })
    })
}

export var remove = function(pos, nChar) {
    return new Promise(function(resolve) {
        updateRemove(pos, nChar).then(function() {
            resolve();
        })
    })
}

export var replace = function(pos, nChar, slot, formatString) {
    return new Promise(function(resolve) {
        updateReplace(pos, nChar, slot, formatString).then(function() {
            resolve();
        })
    })
}

export var performRead = function(lbound, readMode) {
    if(readMode === 'single')    readSentence(lbound)
        else                    readSelection(lbound)
}

export var error = function(text, rate, feedbackRequest) {
    read(text, rate, feedbackRequest)
}

export var undo_redo = function(intent) {
    return new Promise(function(resolve) {
        if (intent == 'undo')
            undoQuill().then(function() { 
                resolve();
            });
        else
            redoQuill().then(function() { 
                resolve();
            });
    })
}