/* imports */ 
import * as tts from './tts.js'
import * as quill from './quill.js'
import {parse, contextMatcher, onDataFromNLPServer} from './parser.js'
import execute from './execute.js'
import genID from './genID.js'
import {error} from './error.js'
import loadJSON from './loadJSON.js'
import * as dom from './dom.js'
import getEditDistance from './levenshtein.js'
import exportToCSV from './exportCSV.js'
import { getListOfKeywords, getphone } from './keyphones.js'

/* socket connection */
export var socket;

/* configure tts player */
tts.setup()

/* working variables */
var speakButton = document.getElementById('rec');
var transcriptFinal = document.getElementById('transcriptFinal');
var transcriptStreaming = document.getElementById('transcriptStreaming');
var logo = document.getElementById('logo');
// var openFile = document.getElementById('open');
// var readFile = document.getElementById("readFile");
// var filePath = document.getElementById("filePath");
var meta = document.getElementById("meta");
var dots = document.getElementById("dots");
var leftBtn = document.getElementById("left");
var rightBtn = document.getElementById("right");

var timeOfLastEvent = new Date().getTime();
var lastDialogflowEvent = new Date().getTime();
var dialogflowEmit = {};
var dialogflowPacket = {}; // 'sent', 'received', 'expired', 'cancelled'
var Emit_Timer_handle = {};
var pingCounter = {};
const MAX_PING_LIMIT = 4;
var dynamicErrorMessageRate = 1;
var utteranceWindowOpen; 
var utteranceWindowHandle;
const utteranceWindowSize = 2000 // 2s
var saveDataTranscript = '';
var saveDataComposeMode = '';
var composedText = '';
var ql = quill.quill;

var testData = {};  // data from input file stored in this object
var nTrials = 0;
var currentObj = {};
var currentTrialObj = {};
var complexityIter = 0;
var complexityLevel = ['_1EU', '_2EU']
var slideIndex = 0;
var showBase = true;
var taskTimer;
var baseToTestTransHandle;
var loadDataHandle;

var activeParticipant;
var activeTechnique;
var isComposition = false; 
var map = {
    0:'keyword', 1:'redict', 2:'combined',
    'keyword':0, 'redict':1, 'combined':2
}

var participantData; // output object for measured data for participant
var nBaseStringRep;
var nReadInstructions;  // no. of read/repeat instructions issued for the working (test) string.
var nAttempts;
var trialCompletionTime;
var utteranceStatus = []
var utteranceHistory = []
var utteranceID;
var statusHandle;
var processedObj = [];

var audioBusy_1 = document.getElementById("audioBusy_1")
var audioBusy_2 = document.getElementById("audioBusy_2")
var waitPhase = 0; 
var audioSent = document.getElementById("audioSent")
var audioBaseString = document.getElementById("audioBaseString")

// should be removed on code release
window.onload = function() {
    // loadData('P1')
    // speakButton.click();
};

function loadData(file, IOString) {
    loadJSON(file, IOString, function(response) {
        // Parse JSON string into object
        if (IOString === 'input') {
            testData = JSON.parse(response);
            console.log('testData loaded ', testData)
        } else if (IOString === 'output') {
            participantData = JSON.parse(response);
            console.log('participantData loaded ', participantData);
        } 
    });
}

function loadDataForTechnique() {
    complexityIter = 0;     // can be only 0 and 1 for 2 EU levels 1EU and 2EU 
    
    currentObj = testData[map[activeTechnique]][complexityLevel[complexityIter]]
    
    nTrials = Object.keys(currentObj).length
    slideIndex = 0;

    console.log('Current Object :: ', currentObj)
    console.log ('nTrials ', nTrials)

    showSlides(slideIndex);
    reset();
}

/* Load respective data set from data.json on sliding over data slides */

/* click on dots for respective slide */
export function currentSlide(n) {
    showSlides(slideIndex = n)
}

// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

function showSlides(n) {
    console.log('n = ', n)
    console.log('complexityIter (before update) :: ', complexityIter)

    if (n == nTrials) {
        console.log('boundary of current difficulty level reached')
        slideIndex = 0
        complexityIter += 1
        if (complexityIter < complexityLevel.length) {
            currentObj = testData[map[activeTechnique]][complexityLevel[complexityIter]]
            nTrials = Object.keys(currentObj).length
        }
    } else if (n < 0) {
        console.log('coming to n < 0 condition')
        complexityIter -= 1
        if (complexityIter >= 0) {
            currentObj = testData[map[activeTechnique]][complexityLevel[complexityIter]]
            nTrials = Object.keys(currentObj).length
            slideIndex = nTrials -1;
        }
    }

    showDots();
    
    console.log('complexityIter (after update) :: ', complexityIter)
    console.log('slide index :: ', slideIndex)
    console.log('complexity Level :: ', complexityLevel[complexityIter])
    console.log ('currentObj keys :: ', Object.keys(currentObj))

    currentTrialObj = Object.values(currentObj)[slideIndex];
    console.log('current trial object :: ', currentTrialObj)

    quill.onLoad()
    quill.setBaseString(currentTrialObj.baseString)

    meta.innerText = currentTrialObj._meta;

    var groupDot = document.getElementsByClassName("dot");
    for (var i = 0; i < groupDot.length; i++) 
        groupDot[i].className = groupDot[i].className.replace(" active", "");

    groupDot[slideIndex].className += " active";
}

function showDots() {
    var childNodes = dots.childNodes
    var nChild = childNodes.length
    console.log ('childNodes = ', childNodes)

    if (childNodes.length)
        for (var i = 0; i < nChild; i++) {
            console.log('removing child ', childNodes[0])
            dom.removeElement(childNodes[0].id)
        }

    for ( var i=0; i < Object.keys(currentObj).length; i++ )
        dom.addElement('dots', 'span', {class: 'dot', id:`dot${i}`}, '');
}

// getter and setter methods // 
export function getSelection(arg) {
    if (arg == 'technique')             return activeTechnique
        else if (arg == 'participant')  return activeParticipant +1 // activeParticipant is 0-indexed
}

/* Event Handlers */

/* core event handler through Start/Pause Button */
$(".primary-btn").on('click', function(e) {     // on click of start button
    if ( !(activeParticipant >= 0 && activeTechnique >= 0) ) {
        if ( !(activeParticipant >= 0) )
            alert('Please select a Participant ID');
        else
            alert('Please select a Technique'); 

        return;
    }
    
    clearPrompt();

    if (showBase) {
        // showBase = false;

        audioBaseString.play()
        tts.read(currentTrialObj.baseString, undefined, 'true') // rate = default; feedbackReq = true;

        baseToTestTransHandle = setTimeout(() => {
            showBase = false;
            quill.onLoad();
            quill.settext(currentTrialObj.testString);
        }, 7000)
        
        return;
    }

    if ( activeTechnique == 2 ) {
        if ( isComposition && !quill.gettext() ) {
            alert('Please compose something before you attempt revision.');
            return; 
        }

        if (isComposition) { 
            isComposition = false;

            composedText = quill.gettext();
            ql.disable();
            quill.onLoad();
            quill.settext(quill.gettext());

            meta.innerText = 'Revision Mode';
        } 
    }
    

    e.preventDefault();
    $("#taskTimer").css("display", "block");
    
    $("#start span.mr-10").toggleClass('mr-11');

    if ( ($("#start span")).hasClass('mr-11') ) {
        $("span.mr-10").html('Pause')
        $("span.lnr").addClass('fa fa-pause')

        // taskTimer = new Timer();
        taskTimer.start({precision: 'secondTenths'});   // tenth of Seconds <easytimer.js>
        taskTimer.addEventListener('secondTenthsUpdated', function (e) {
            $('#taskTimer .values').html(taskTimer.getTimeValues().toString(['hours', 'minutes', 'seconds', 'secondTenths']));
        });

        transcriptStreaming.innerText = '';

        if (!speakButton.checked)
            speakButton.click();    // turn on
        
        tts.readSelection(0);
    }

    else {      // on pause
        $("span.mr-10").html('Start')
        $("span.lnr").removeClass('fa fa-pause')

        trialCompletionTime = taskTimer.getTimeValues().toString(['hours', 'minutes', 'seconds', 'secondTenths'])
        console.log(trialCompletionTime)       
        
        taskTimer.pause()
        clearTimeout(loadDataHandle);

        loadData(`P${activeParticipant +1}`, 'output');

        loadDataHandle = setTimeout( () => {
            console.log('retrieved participant data ', participantData);
            prepareWriteObj(participantData)
                .then( (data) => {
                    socket.emit('write', {data, participantID:`P${activeParticipant +1}`, path:'output'})
                })

            if (tts.isPlaying())        
                tts.pause(); 
            pauseBusyAudio();
            
        }, 400)
    }
});


/* Dropdown selectors - get parameters for the participant & technique */
$('#training').bind('click', function(e) {
    $("#participant a.training").toggleClass('training_show');
    $('#training').toggleClass('clicked')
    clearPrompt();
})

$('#participant a').bind('click', function(e){
    if ( e.target !== this )
        return;

    if (activeParticipant >= 0)
        $("#participant a").eq(activeParticipant).toggleClass('dropdown-item-checked')
    var index = $('#participant a').index(this)
    $("#participant a").eq(index).toggleClass('dropdown-item-checked')
    activeParticipant = index;
    console.log('activeParticipant ', activeParticipant +1)
    loadData(`P${index +1}`, 'input');                           // load input data
    loadData(`P${index +1}`, 'output');                           // load output data
});

$('.buttonDownload').bind('click', function(){
    var index = $('.buttonDownload').index(this)
    console.log('index ', index)
    participantData = '';
    loadData(`P${index +1}`, 'output');
    setTimeout(() => {
        if ( !participantData ) {
            alert(`No data found for Participant ${index +1}`);
            return;
        }

        if ( !speakButton.checked )
            speakButton.click() 

        var participantIDSelectForExport = `P${index +1}`
        processRawJSON(participantIDSelectForExport)
            .then( (data) => {
                socket.emit('write', {data, participantID:`P${index +1}`, path:'processed'})
            })
            .catch(() => alert('Sorry, unsupported file format. Download aborted!')); 
    }, 200)
});


$('#technique a').bind('click', function(){
    if ( !(activeParticipant >= 0) ) {
        alert('Please select a Participant ID first!');
        return;
    }

    if (activeTechnique >= 0)
        $("#technique a").eq(activeTechnique).toggleClass('dropdown-item-checked')
    var index = $('#technique a').index(this)
    $("#technique a").eq(index).toggleClass('dropdown-item-checked')
    activeTechnique = index;

    console.log(testData.techniqueOrder)
    if ( !participantData && map[testData.techniqueOrder[0]] != activeTechnique )
        alert('Please be aware that you are violating the counter-balanced order');

    if (activeTechnique == 1)
        alert('Please check if CORS is Enabled.')

    if (activeTechnique < 2)
        loadDataForTechnique();

    else if (activeTechnique == 2) {  // enable the quill for recording dictation, on press of Start button disable quill and start revision
        if ( ($("#start span")).hasClass('mr-11') ) {   // timer is still running
            alert('Please stop the timer for the current trial. Else, you will lose the data.')
            return;
        }

        initComposition();
    }

});

function initComposition() {
    reset();

    ql.enable();
    quill.onLoad();
    quill.settext('');

    clearPrompt();
    isComposition = true;
    showBase = false;

    taskTimer = new Timer()
    $("#taskTimer").css("display", "none");

    meta.innerText = 'Composition Mode';
}

/* Left and Right Sliders */
leftBtn.addEventListener('click', function(e) {
    if ( complexityIter == 0 && slideIndex == 0 )   return;
    if ( activeTechnique == 2 ) return;

    if ( ($("#start span")).hasClass('mr-11') ) {
        $(".primary-btn").click();
    }

    plusSlides(-1)
    reset();
});

rightBtn.addEventListener('click', function(e) {
    if ( complexityIter == complexityLevel.length -1 && slideIndex == nTrials -1 )  return;
    if ( activeTechnique == 2 ) return;

    if ( ($("#start span")).hasClass('mr-11') ) {
        $(".primary-btn").click();
    }

    plusSlides(+1)
    reset();
});

const reset = () => {
    showBase = true;
    clearPrompt();

    taskTimer = new Timer()
    $("#taskTimer").css("display", "none");

    nBaseStringRep = 0;
    nReadInstructions = 0;
    nAttempts = 0;

    utteranceStatus = [];
    utteranceHistory = [];
    utteranceID = 0;

    transcriptStreaming.innerText = '';
}

logo.addEventListener('click', function(e) {
    audioBaseString.play()
    tts.read(currentTrialObj.baseString, undefined, 'true') // rate = default; feedbackReq = true;
    if (!showBase)
        nBaseStringRep += 1; 
})

/* File Uploader */
// openFile.addEventListener('click', function(e) {
//     readFile.click();
// });

// readFile.onchange = function () {
//     var fileName = readFile.value.split('\\')[readFile.value.split('\\').length - 1];
//     filePath.innerHTML = "<b>Selected File: </b>" + fileName;
//     var reader = new FileReader();
//     reader.onload = function(e){
//         // console.log(reader.result.substring(0, 200));
//         $(".menu-bar").click();
//         quill.onLoad()
//         quill.settext(reader.result)
//     };
//     reader.readAsText(readFile.files[0]);
// };

/* emit rec. event when recorder button is on */
speakButton.addEventListener('click', function(){
    if (speakButton.checked) {
        console.log('Recorder On')             
        socket = io.connect('http://localhost:7000');
        socket.on('connect', listen);
    }
    else {
        console.log('Disconnecting socket', socket.id);
        socket.destroy();
    }
});

document.getElementById('editor').addEventListener('click', (e) => {
    pauseBusyAudio();
    if (e.metaKey)
        tts.readSelection(quill.quill.getSelection().index);
});

document.addEventListener('click', (e) => {
    pauseBusyAudio();
    if (tts.isPlaying())
        tts.pause();                // pause on Click
});

document.addEventListener('dblclick', (e) => {
    // pauseBusyAudio();
    // tts.readSelection(quill.quill.getSelection().index);

    // if (speakButton.checked)
    //     speakButton.click()     // turn off. => dblclick cannot be used to turn on. only for turn off.

    // ! Force socket to turn off only through the start/pause interface
    if ( ($("#start span")).hasClass('mr-11') ) {
        $(".primary-btn").click();
    }
});

document.addEventListener('keypress', (e) => {
    console.log('key pressed ', e.keyCode);
    
    var show = false;
    clearTimeout(statusHandle);

    var $div = $("#status");
    statusHandle = setTimeout(function() {
        $div.fadeOut(500);
    }, 2000);

    switch(e.keyCode) {
        case 101:   // e
            if (!ql.hasFocus()) {
                utteranceStatus.push('Sys. Error')
                utteranceHistory[utteranceID -1].status = 'Sys. Error'
                show = true;
            }
            break;
        case 114:   // r
            if (!ql.hasFocus()) {
                utteranceStatus.push('Rec. Error')
                utteranceHistory[utteranceID -1].status = 'Rec. Error'
                show = true;
            }
            break;
        case 116:   // t
            if (!ql.hasFocus()) {
                utteranceStatus.push('User Error')
                utteranceHistory[utteranceID -1].status = 'User Error'
                show = true;
            }
            break;
        case 102:   // f
            if (!ql.hasFocus()) {
                utteranceStatus.push('Success')
                utteranceHistory[utteranceID -1].status = 'Success'
                show = true;
            }
            break;
        case 100:   // d
            if (!ql.hasFocus()) {
                utteranceStatus.pop()
                if ( utteranceHistory[utteranceID -1].status )
                    utteranceHistory[utteranceID -1].status = undefined
                show = true;
            }
            break;
        case 115:
            if (!ql.hasFocus())
                show = true;
            break;
        
        case 96:    // ~
            if (!ql.hasFocus())     ql.enable()
                else                ql.disable();
            break;

        case 32:
            if (showBase) {
                if ( tts.isPlaying() )
                    tts.pause();

                showBase = false;
                clearTimeout(baseToTestTransHandle);
                quill.onLoad();
                quill.settext(currentTrialObj.testString);
            }
            break;
            
    }

    if (!show)
        return;

    // show last 3 status briefly on screen
    $($div).html(utteranceStatus.slice(-3).join(' | '));
    $div.fadeIn(200);

}); 


// Listen for socket events
function listen() {
    socket.on('rec', function(data){
        console.log('Reply received on', socket.id);
        // data.results.map(x => x.alternatives.map(y => console.log(y)))

        // combined mode only
        if ( activeTechnique == 2 && isComposition ) {
            transcriptStreaming.innerText = data.transcript;

            if ( data.isFinal && data.transcript != saveDataComposeMode ) {
                saveDataComposeMode = data.transcript;
                ql.insertText(ql.getLength() -1, data.transcript)
                transcriptStreaming.innerText = '';
            }

            return;
        }

        
        if (tts.isPlaying())        // pause on Speech
            tts.pause(); 

        pauseBusyAudio();

        transcriptStreaming.innerText = data.transcript;

        if ( data.isFinal && (new Date().getTime() - timeOfLastEvent > 800) && data.transcript != saveDataTranscript ) {
            timeOfLastEvent = new Date().getTime()
            // ! Remove this later - just for the purpose of the experiment
            // saveDataTranscript = data.transcript

            transcriptFinal.innerText += transcriptStreaming.innerText + ' '
            transcriptStreaming.innerText = ''

            // // ! Experimental section (improve recognition)
            // if ( activeTechnique != 1 ) {
            //     transcriptFinal.innerText = transcriptFinal.innerText.replace(/\./, '');
            //     var split_ = transcriptFinal.innerText.trim().split(' ');
            //     var keyFound = false;
            //     // var deltaTolerance = (activeTechnique == 0) ?2 :1;
            //     var deltaTolerance = 1;
            //     var keywords = getListOfKeywords();
            //     for ( var tokenIndex = 1; tokenIndex < split_.length; tokenIndex++ ) {
            //         let x = split_[tokenIndex]; 
            //         let saveToken = split_[tokenIndex -1];
            //         console.log('token ', x)

            //         if ( keywords.findIndex(key => x === key || saveToken === key) >= 0 )
            //             break;

            //         let compound = saveToken + ' ' + x;
            //         for ( var phoneIndex = 0; phoneIndex < keywords.length; phoneIndex++ ) {
            //             let y = keywords[phoneIndex]
            //             console.log(`matching "${compound}" against keyphone of "${y}"`)
            //             console.log(`edit dist. between "${compound}" and "${y}" = ${getEditDistance(clj_fuzzy.phonetics.caverphone(compound), getphone(y))}`)
            //             if (getEditDistance(clj_fuzzy.phonetics.caverphone(compound), getphone(y)) <= deltaTolerance) {
            //                 console.log(`replacing "${compound}" with "${y}"`)
            //                 var re = new RegExp('\\b' + compound + '\\b', 'gi')
            //                 transcriptFinal.innerText = transcriptFinal.innerText.replace(re, y)
            //                 keyFound = true;
            //                 break;
            //             } 
            //         }
                    
            //         if (keyFound)
            //             break; 
            //     }

            //     if (!keyFound) {
            //         for ( var tokenIndex = 0; tokenIndex < split_.length; tokenIndex++ ) {
            //             let x = split_[tokenIndex];
            //             for ( var phoneIndex = 0; phoneIndex < keywords.length; phoneIndex++ ) {
            //                 let y = keywords[phoneIndex]
            //                 console.log(`matching "${x}" against keyphone of "${y}"`)
            //                 console.log(`edit dist. between "${x}" and "${y}" = ${getEditDistance(clj_fuzzy.phonetics.caverphone(x), getphone(y))}`)
            //                 if ( getEditDistance(clj_fuzzy.phonetics.caverphone(x), getphone(y)) <= deltaTolerance ) {
            //                     console.log(`replacing "${x}" with "${y}"`)
            //                     var re = new RegExp('\\b'+x+'\\b', 'gi')
            //                     transcriptFinal.innerText = transcriptFinal.innerText.replace(re, y)
            //                     keyFound = true;
            //                     break;
            //                 }
            //             }

            //             if (keyFound)
            //                 break;
            //         }
            //     }

            //     console.log('processed transcript ', transcriptFinal.innerText);

            // }
            // // ! End of Experimental Section
            
            console.log('Opening Utterance Window') 
            utteranceWindowOpen = true;
            utteranceWindowHandle = setTimeout( () => {
                var uuid = genID()
                socket.emit('dialogflow', {data: transcriptFinal.innerText.trim(), uuid})

                utteranceID += 1;
                utteranceHistory.push(
                    {   
                        utteranceID,
                        utterance: transcriptFinal.innerText.trim(),
                        timestamp: taskTimer.getTimeValues().toString(['hours', 'minutes', 'seconds', 'secondTenths']),
                        status: undefined
                    }
                )

                waitPhase = 0; pingCounter[uuid] = 0;
                StartTimer(uuid, transcriptFinal.innerText.trim())

                console.log('Closing Utterance Window') 
                utteranceWindowOpen = false
                transcriptStreaming.innerText = transcriptFinal.innerText
                transcriptFinal.innerText = ''
                
                audioSent.play()
                nAttempts += 1;

            }, utteranceWindowSize)
        }
        else if ( !data.isFinal && utteranceWindowOpen ) {
            clearTimeout(utteranceWindowHandle)
            utteranceWindowOpen = false;
            console.log('Window stop on user interrupt')
        }
    });

    socket.on('analysis', function(data) {
        onDataFromNLPServer(data.tokens).then(function(tokens) {
            contextMatcher(tokens, data.locationContext).then(function(operation) {
                console.log('operation received in main :: ', operation)
                if (operation.status === 'error')
                    execute(operation)
                else
                    execute(operation).then(function() {
                        // ! Just for the purpose of the study
                        if ( activeTechnique < 2 )
                            operation.ttsResumeAt = 0;

                        if (tts.getReadMode() === 'burst') {
                            console.log('burst')
                            console.log('operation.ttsResumeAt = ', operation.ttsResumeAt)
                            tts.readSelection(operation.ttsResumeAt);
                        }
                        else {
                            console.log('single')
                            console.log('operation.ttsResumeAt = ', operation.ttsResumeAt)
                            tts.readSentence(operation.ttsResumeAt);
                        }
                    })

            })
        })
    })

    socket.on('dialogflowresponse', function(data) {
        if (new Date().getTime() - lastDialogflowEvent > 800) {
            if (dialogflowPacket[data.uuid] === 'sent') {
                console.log('response from dialogflow server :: ',data)

                dialogflowPacket[data.uuid] = 'received'
                console.log(`Response RECEIVED for Dialogflow packet with UUID :: ${data.uuid}`)
                console.log(`Response Time :: ${new Date().getTime() - dialogflowEmit[data.uuid]}`)
                
                pauseBusyAudio();
                callParser(data)
            }
        }
    })

    socket.on('dialogflowError', function(data) {
        abort(data)
    })

    // socket.on('clean', data => {
    //     quill.clean();   // commented for testing - to be removed later
    //     console.log('fired on clean')
    //     quill.settext(data)  // commented for testing - to be removed later
    // })

    // socket.on('read', function(data) {
    //     participantData = data;
    //     console.log('retrieved participantData (on socket reception) ', participantData);
    // })

    socket.on('writeSuccess', function(data) {
        if (speakButton.checked)
            speakButton.click();    // turn off. 

            if (data.path === 'processed')
                exportProcessedDataToCSV(data.participantID)
    })

}

function callParser(data) {
    parse(data, tts.getLocationContext()).then(function(operation) {
        console.log('operation received in main :: ', operation)

        if (operation.status == 'error') {
            execute(operation)
        }
        else {
            if (operation.intent === 'read' || operation.intent === 'repeat')
                nReadInstructions += 1;
            execute(operation).then(function() {
                // ! Just for the purpose of the study
                if ( activeTechnique < 2 )
                    operation.ttsResumeAt = 0;

                if (tts.getReadMode() === 'burst') {
                    console.log('burst')
                    console.log('operation.ttsResumeAt = ', operation.ttsResumeAt)
                    tts.readSelection(operation.ttsResumeAt);
                }
                else {
                    console.log('single')
                    console.log('operation.ttsResumeAt = ', operation.ttsResumeAt)
                    tts.readSentence(operation.ttsResumeAt);
                }
            })
            
        }
    })
}

function StopTimer(uuid, data) {
    clearTimeout(Emit_Timer_handle[uuid]);

    if (dialogflowPacket[uuid] === 'sent') {
        if ( pingCounter[uuid] < MAX_PING_LIMIT ) {
            dialogflowPacket[uuid] = 'expired'
            console.log(`Timer EXPIRED for Dialogflow packet with UUID :: ${uuid}`)
            
            // var reping_uuid = genID();          // re-ping with new uuid
            var reping_uuid = uuid;           // re-ping with the same uuid
            
            socket.emit('dialogflow', {data, uuid: reping_uuid})
            StartTimer(reping_uuid, data)

            playBusyAudio();
        }

        else
            abort( {utterance: data, uuid} )

    }

}

function StartTimer(uuid, data) {
    var time_Quantum = 700     // 1500ms
    console.log('Timer started for Dialogflow packet with UUID :: ', uuid)

    Emit_Timer_handle[uuid] = setTimeout(StopTimer, time_Quantum, uuid, data)
    dialogflowPacket[uuid] = 'sent'
    dialogflowEmit[uuid] = new Date().getTime();
    pingCounter[uuid] += 1;
}

function playBusyAudio() {
    var delay = 4     // one play of audio ever delay no. of time quantums 
    waitPhase = (waitPhase + 1) % (delay * 2)   // as alternating between two separate audios

    if (waitPhase == delay -1)
        audioBusy_1.play()
    else if (waitPhase == 2*delay -1)
        audioBusy_2.play()
}

function pauseBusyAudio() {
    audioBusy_1.pause(); 
    audioBusy_2.pause();
}

function abort(data) {
    var operation = {
        status: 'error', 
        errorMessage: {
            text: error(
                {param: data.utterance}
            )['OPERATION_ABORTED'],
            rate: dynamicErrorMessageRate
        }
    }
    
    console.log('ERROR response from the Dialogflow server');

    dialogflowPacket[data.uuid] = 'cancelled'
    pauseBusyAudio();
    
    execute(operation)
}

function prepareWriteObj(obj) {
    obj = obj || {}
    return new Promise(function(resolve) {
        obj.participantID = `P${activeParticipant +1}`
        
        var tech = map[activeTechnique];
        
        obj[tech] = obj[tech] || {}

        if (tech === 'combined') {
            obj[tech].T1 = obj[tech].T1 || []
            obj[tech].T1.push(
                {
                    dictatedText: composedText,
                    resultString: quill.gettext(),
                    dictated_to_result_dist: getEditDistance(composedText, quill.gettext()),
                    nReadInstructions,
                    trialCompletionTime,
                    nAttempts,
                    utteranceHistory
                    // utteranceStatus
                }
            )

            setTimeout(() => {
                resolve(obj);
            }, 1000);

        } else {
            obj[tech][`_${complexityIter +1}EU`] = obj[tech][`_${complexityIter +1}EU`] || {}
            obj[tech][`_${complexityIter +1}EU`][`T${slideIndex +1}`] = obj[tech][`_${complexityIter +1}EU`][`T${slideIndex +1}`] || []

            obj[tech][`_${complexityIter +1}EU`][`T${slideIndex +1}`].push(
                {
                    _meta: currentTrialObj._meta,
                    config: currentTrialObj.config || undefined,
                    trigger: currentTrialObj.trigger,
                    baseString: currentTrialObj.baseString,
                    testString: currentTrialObj.testString,
                    resultString: quill.gettext(),
                    base_to_test_dist: getEditDistance(currentTrialObj.baseString, currentTrialObj.testString),
                    base_to_result_dist: getEditDistance(currentTrialObj.baseString, quill.gettext()), 
                    nBaseStringRep,
                    nReadInstructions,
                    trialCompletionTime,
                    nAttempts,
                    utteranceHistory
                    // utteranceStatus
                }
            )

            setTimeout(() => {
                resolve(obj);
            }, 1000);

        }
    })
}

function processRawJSON(participantID) {
    processedObj = [];

    return new Promise(function(resolve, reject) {
        console.log('participantData ', participantData)
        console.log('participantData.keyword ', participantData.keyword)
        console.log('participantData.keyword._2EU ', participantData.keyword._2EU)

        var keywordObj_1EU = participantData.keyword._1EU;
        var keywordObj_2EU = participantData.keyword._2EU;

        var redictObj_1EU = participantData.redict._1EU;
        var redictObj_2EU = participantData.redict._2EU;

        try {
            Object.keys(keywordObj_1EU).map( x => {
                var lastElKey = keywordObj_1EU[x][keywordObj_1EU[x].length -1]

                processedObj.push(
                    {   
                        participantID,
                        ErrorUnits: 1,
                        trial: x,
                        trigger: lastElKey.trigger,
                        Technique: 'Keyword-Based',
                        TrialCompletionTime: convertToSeconds(lastElKey.trialCompletionTime),
                        NumberOfAttempts: lastElKey.nAttempts,
                        BaseStringRepeats: lastElKey.nBaseStringRep,
                        ReadInstructions: lastElKey.nReadInstructions,
                        ErrorIndex: lastElKey.base_to_result_dist / lastElKey.base_to_test_dist
                    }
                )
            })
        } catch (err) {
            reject();
        }

        try {
            Object.keys(redictObj_1EU).map( x => {
                var lastElRed = redictObj_1EU[x][redictObj_1EU[x].length -1]

                processedObj.push(
                    {   
                        participantID,
                        ErrorUnits: 1,
                        trial: x,
                        trigger: lastElRed.trigger,
                        Technique: 'Re-Dictation',
                        TrialCompletionTime: convertToSeconds(lastElRed.trialCompletionTime),
                        NumberOfAttempts: lastElRed.nAttempts,
                        BaseStringRepeats: lastElRed.nBaseStringRep,
                        ReadInstructions: lastElRed.nReadInstructions,
                        ErrorIndex: lastElRed.base_to_result_dist / lastElRed.base_to_test_dist
                    }
                )
            })
        } catch (err) {
            reject();
        }

        try {
            Object.keys(keywordObj_2EU).map( x => {
                var lastElKey = keywordObj_2EU[x][keywordObj_2EU[x].length -1]

                let split_; 
                if (lastElKey.trigger)
                    split_ = lastElKey.trigger.split('')
                else split_ = '';

                let category;
                if (split_[0] === split_[1])
                    category = 'Same'

                processedObj.push(
                    {   
                        participantID,
                        ErrorUnits: 2,
                        trial: x,
                        config: lastElKey.config,
                        trigger: lastElKey.trigger,
                        trigger_category: category || 'Different',
                        Technique: 'Keyword-Based',
                        TrialCompletionTime: convertToSeconds(lastElKey.trialCompletionTime),
                        NumberOfAttempts: lastElKey.nAttempts,
                        BaseStringRepeats: lastElKey.nBaseStringRep,
                        ReadInstructions: lastElKey.nReadInstructions,
                        ErrorIndex: lastElKey.base_to_result_dist / lastElKey.base_to_test_dist
                    }
                )
            })
        } catch (err) {
            reject();
        }
        
        try {
            Object.keys(redictObj_2EU).map( x => {
                var lastElRed = redictObj_2EU[x][redictObj_2EU[x].length -1]

                let split_; 
                if (lastElRed.trigger)
                    split_ = lastElRed.trigger.split('')
                else split_ = '';

                let category;
                if (split_[0] === split_[1])
                    category = 'Same'

                processedObj.push(
                    {   
                        participantID,
                        ErrorUnits: 2,
                        trial: x,
                        config: lastElRed.config,
                        trigger: lastElRed.trigger,
                        trigger_category: category || 'Different',
                        Technique: 'Re-Dictation',
                        TrialCompletionTime: convertToSeconds(lastElRed.trialCompletionTime),
                        NumberOfAttempts: lastElRed.nAttempts,
                        BaseStringRepeats: lastElRed.nBaseStringRep,
                        ReadInstructions: lastElRed.nReadInstructions,
                        ErrorIndex: lastElRed.base_to_result_dist / lastElRed.base_to_test_dist
                    }
                )
            })
        } catch (err) {
            reject();
        }

        setTimeout(() => {
            resolve(processedObj);
        }, 1000);

    })
    
}

function exportProcessedDataToCSV(fileName) {
     /* export to csv */
    var headers_1EU = {
        participantID: 'Participant ID',
        ErrorUnits: '#Error Units',
        Technique: "Technique",
        trial: 'Trial ID',
        trigger: "Trigger",
        TrialCompletionTime: "Trial Completion Time (in sec.)",
        NumberOfAttempts: "#Attempts",
        BaseStringRepeats: "#Base Text Repeats",
        ReadInstructions: "#Read Requests",
        ErrorIndex: "Error Index"
    };

    var headers_2EU = {
        participantID: 'Participant ID',
        ErrorUnits: '#Error Units',
        Technique: "Technique",
        trial: 'Trial ID',
        config: "Trigger Embed Configuration",
        trigger: "Trigger",
        trigger_category: "Trigger Category",
        TrialCompletionTime: "Trial Completion Time (in sec.)",
        NumberOfAttempts: "#Attempts",
        BaseStringRepeats: "#Base Text Repeats",
        ReadInstructions: "#Read Requests",
        ErrorIndex: "Error Index"
    };

    var processedObjFormatted_1EU = [];
    var processedObjFormatted_2EU = [];
    
    // format the data
    processedObj.forEach((trialObj) => {
        if (trialObj.ErrorUnits == 2)
            processedObjFormatted_2EU.push({
                participantID: trialObj.participantID,
                ErrorUnits: trialObj.ErrorUnits,
                Technique: trialObj.Technique,
                trial: trialObj.trial || '',
                config: trialObj.config || '',
                trigger: trialObj.trigger || '',
                trigger_category: trialObj.trigger_category || '',
                TrialCompletionTime: trialObj.TrialCompletionTime,
                NumberOfAttempts: trialObj.NumberOfAttempts,
                BaseStringRepeats: trialObj.BaseStringRepeats,
                ReadInstructions: trialObj.ReadInstructions,
                ErrorIndex: trialObj.ErrorIndex
            });
        else if (trialObj.ErrorUnits == 1) {
            if (trialObj.trial !== 'T4' && trialObj.trial !== 'T5' && trialObj.trial !== 'T6')
                processedObjFormatted_1EU.push({
                    participantID: trialObj.participantID,
                    ErrorUnits: trialObj.ErrorUnits,
                    Technique: trialObj.Technique,
                    trial: trialObj.trial || '',
                    trigger: trialObj.trigger || '',
                    TrialCompletionTime: trialObj.TrialCompletionTime,
                    NumberOfAttempts: trialObj.NumberOfAttempts,
                    BaseStringRepeats: trialObj.BaseStringRepeats,
                    ReadInstructions: trialObj.ReadInstructions,
                    ErrorIndex: trialObj.ErrorIndex
                });
        }
    });
    
    console.log('processedObject for 1EU ', processedObjFormatted_1EU)
    console.log('processedObject for 2EU ', processedObjFormatted_2EU)

    exportToCSV(headers_1EU, processedObjFormatted_1EU, fileName+'_1EU');
    exportToCSV(headers_2EU, processedObjFormatted_2EU, fileName+'_2EU');
    
}

function clearPrompt() {    // clear any residual transcript prompt from before
    transcriptFinal.innerText = '';
    transcriptStreaming.innerText = '';
}

function convertToSeconds(time) {
    var split_ = time.split(':')
    let seconds = parseInt(split_[0])*3600 + parseInt(split_[1])*60 + parseInt(split_[2])
    if ( parseInt(split_[3]) >= 5 )
        seconds += 1;
    
    return seconds;
}