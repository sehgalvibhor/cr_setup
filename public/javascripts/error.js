const tokenMap = {
    'beginning': 'starting',
    'end': 'ending',
    'insert': 'Insertion',
    'delete': 'Deletion',
    'replace': 'Replace operation',
    'undo': 'Undo operation',
    'redo': 'Redo operation',
    

    '': ''
}

export const error = function(params) {  
    params = params || ''
    return(
        {
            'MATCH_AT_BOUNDARY_NOT_FOUND': `Couldn\'t find a line ${tokenMap[params.param]} with ${params.target}.`,
            'SENTENCE_INDEX_OUT_OF_BOUND': `There are only ${params.param} sentences.`,
            'REF_NOT_FOUND': `I could not identify where to ${params.param}. Please be more specific.`,
            'KEYWORD_NOT_FOUND': `Did you want me to ${params.param} something? Please try again.`,
            'SLOT_NOT_FOUND': `The ${tokenMap[params.param]} failed. I am not sure what you want me to ${params.param}.`,
            'TARGET_NOT_FOUND': `Couldn\'t find a match for ${params.target}.`,
            'CONTAINER_TARGET_NOT_FOUND': `No match for sentence having ${params.target} in it.`,
            'OPERATION_FAILED': `Sorry, the ${tokenMap[params.param]} failed. Please try again.`,
            'OPERATION_ABORTED': `Sorry, your instruction failed. What I heard was "${params.param}". Did you say something else? Please retry.`,
            'BLANK_QUILL': `There is no text in the editor.`,
            'PREV_NOT_FOUND': `This is the first sentence`,
            'NEXT_NOT_FOUND': `There are no more sentences after this`,
            'NO_INTENT_MATCHED': `Sorry, I failed to pick an intent. What I heard was "${params.param}". Could you please retry?.`,
            'KEYWORD_NOT_ALLOWED': `Sorry, in this mode you cannot use keywords for insertion, deletion or replacement. Try dictating the correct phrase instead.`,

        }
    )
}
