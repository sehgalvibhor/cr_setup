export default function loadJSON(file, IOString, callback) {   

    var xhr = new XMLHttpRequest();
    xhr.overrideMimeType("application/json");
    xhr.open('GET', '/data/'+IOString+'/'+file+'.json', true); // Replace 'my_data' with the path to your file
    xhr.onreadystatechange = function () {
          if (xhr.readyState == 4 && xhr.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xhr.responseText);
          }
    };
    xhr.onerror = function(){console.log("error" + xhr.status); throw 404}  
    xhr.send(null);  
} 
