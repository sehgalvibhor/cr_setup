export const left = function(a, b) {    // searches for overlap between any part of b(:=query) with a(:= target)
  /* get left overlap */

    if (b.length == 0) {
      return {content: "", location: -1}
    }

    var re = new RegExp('\\b'+b.trim()+'\\b', 'gi');
    var index = -1, match;
    while ((match = re.exec(a)) !== null)
        index = match.index
    
    if (index >= 0) {
      return {
          content: b.trim(),
          location: index
      }
    }
    
    return left(a, b.substring(0, b.lastIndexOf(' ')));
}

export const right = function(a, b) {
  /* get right overlap */
    
    var re = new RegExp('\\b'+b.trim()+'\\b', 'gi');
    var index = -1, match;
    while ((match = re.exec(a)) !== null)
        index = match.index
    
    if (index >= 0) {
      return {
          content: b.trim(),
          location: index
      }
    }

    if (b.indexOf(' ') == -1) {
      return {content: "", location: -1}
    }
    
    return right(a, b.substring(b.indexOf(' ')+1, b.length));
}
