import * as overlap from './overlap.js'
import {setLocationContext, read} from './tts.js'
import { isValid } from './CheckDef.js';

/* Quill editor setup */
export var quill = new Quill('#editor', {
    modules: {
        history: {
            delay: 1500,
            maxStack: 100,
        }
    }
})
var Delta = Quill.import('delta');
quill.disable();

// temporary for testing
// onLoad();
// settext(exp);

/* working variables */
var sentence_boundary = []
var textInPlay = ''
var ttsResumeContextLength = 25 // the no. of chars that the tts should rewind back while resuming reading of the text after pause
var APIOperationIndex; 
var textChangeEventType = 'normal';

let updateCompleted = function() {
    return new Promise(function(resolve) {
        resolve();
    })
}

quill.on('text-change', function(newDelta, oldDelta, source) {
    console.log('on text-change fired')
    if (textChangeEventType === 'normal') {

        console.log('text-change event fired :: normal')
        console.log('quill.getContents :: ', quill.getContents())
        textChangeEventType = 'clean';

        console.log('oldDelta ', oldDelta)
        console.log('newDelta ', newDelta)

        // var re = /\s+(?=\.)|\s(?!\b)|[^A-Za-z0-9\.\s]/g
        // settext(gettext().replace(re, ''))

        setLocationContext(newDelta.ops[0].retain, 'char')
        setAPIOperationIndex(newDelta.ops[0].retain)

        updateCompleted();

    }

    else if (textChangeEventType === 'clean') {
        console.log('fired on clean')
        textChangeEventType = 'normal'
    }

    else if (textChangeEventType === 'onLoad') {
        console.log('fired on quill load')
        textChangeEventType = 'normal'
    }

})


/* Getter & Setter Methods */
export function gettext() {
    quill.focus()
    return quill.getText().trim()
}

export function settext(text) {
    quill.focus()

    text = text.replace(/[?!;]/g, '.')
    text = text.replace(/(\b|\s+)$/g, '.')
    text = text.replace(/\s+(?=\.)|\s(?!\b)|[^A-Za-z0-9\.\s]/g, '')
    // text = text.replace(/\b(and|but|although|because|either|however|if|in case|in spite of|neither|or|otherwise|since|unless|whether|nor)\b\s+/g, '')

    quill.setText(text)
}

export function setBaseString(text) {
    quill.focus();
    quill.updateContents(new Delta()
        .delete(quill.getLength())
        .insert(text, {'color': 'yellow'})
    );
}

export function getAPIOperationIndex()  { // for history module, undo / redo (user does not directly manipulate the char index)
    return APIOperationIndex; 
}

const setAPIOperationIndex = function(value) {
    APIOperationIndex = value; 
}

export function onLoad() {
    textChangeEventType = 'onLoad'
}

/* Get Effective Search String for Scope of Operation */
function getTextInPlay(locationContext) {
    console.log('pause location in getTextInPlay', locationContext)

    // if (getReadMode() == 'single') {
    //     var sent_boundary = sentIndexToCharIndexBounds(locationContext)
    //     textInPlay = quill.getText(sent_boundary.lbound +2, sent_boundary.ubound +1)
    // } 
    
    console.log('locationContext in getTextInPlay() = ', locationContext)
    if ( !isValid(locationContext) || locationContext <= 0)
        textInPlay = quill.getText().trim();
    else
        textInPlay = quill.getText(0, getDelimiterPositionsList()[locationContext]+1)

    console.log('textInPlay', textInPlay)
    return textInPlay
}




/* get Sentence Boundary Points (positions of Delimiters) */
export function getDelimiterPositionsList() {
    var re = /(\b[.?!](?=\s+))|(\b[.?!]$)/g;
    var content = quill.getText().trim()
    var match = 0;

    sentence_boundary = [] // purge any previous content.

    while ((match = re.exec(content)) !== null) {
        // console.log(`Found ${match[0]} at ${re.lastIndex-1}.`);
        sentence_boundary.push(re.lastIndex-1)
    }

    if ( sentence_boundary.length == 0)
        sentence_boundary.push(quill.getLength() - 1)
    else if ( /\w/.test(quill.getText().trim().slice(-1)) )
        sentence_boundary.push(quill.getLength() - 1)

    console.log('sentence Bounds', sentence_boundary)
    return sentence_boundary
}


/* set TTS Resume Char Index */ 
export function getResumeAtCharIndex(operationIndex) {  // start char Index of operation executed
    console.log('inside getResumeAtCharIndex()')
    var extract = quill.getText(operationIndex - ttsResumeContextLength, ttsResumeContextLength)
    console.log('operationIndex = ', operationIndex)
    console.log('extract = ', extract)
    var re = /.+\.\s*(\b\w)*/
    var match = re.exec(extract)
    console.log('match = ', match)
    if (!match)
        return operationIndex - ttsResumeContextLength
    else
        return operationIndex - ttsResumeContextLength + match[0].length - (match[1] ?match[1].length :0)
}



/* char Index (absolute) <-> sent Index (relative) mappings and index to String mappings  */
export function charIndexToCharIndexBounds(pos) {   // given char index get lower and upper char index bounds of enclosing sentence
    var delimiterPosList = getDelimiterPositionsList();
    var i = 0;
    var lbound = 0;
    var ubound;

    while ( (ubound = delimiterPosList[i++]) < pos)
        lbound = ubound;

    return {lbound, ubound};    // lbound points to delimiter of prev. sentence - (unrefined)
}

export function sentIndexToCharIndexBounds(locationContext) { // given sentence index get char index bounds of [prev, this, next] neighbourhood & #sentences count in the _2nn object
    console.log('paused at sentence ', locationContext)

    var delimiterPosList = getDelimiterPositionsList();
    var _2nn = {    // has the upper bounds in the pre_prev, prev, this and next keys. so this has upper bound of the current sentence index
        pre_prev: delimiterPosList[locationContext -2],
        prev: delimiterPosList[locationContext - 1], 
        this: delimiterPosList[locationContext],
        next: delimiterPosList[locationContext + 1],
        count: delimiterPosList.length
    }

    console.log('_2nn :: ', _2nn)

    return _2nn;
}


export function getSentence(start_index, end_index) {   // given start & end char index get Sentence text and sentence start char index
    console.log('start_index from quill :: ', start_index)
    console.log('end_index from quill :: ', end_index)
    var content = quill.getText().trim()
    var extract = content.substring(start_index, end_index+1)
    console.log('extract from quill :: ', extract)
    var nearestAlphabetIndex = (/\b/).exec(extract).index
    console.log('nearest alphabet index :: ', nearestAlphabetIndex)

    return {
        text: extract.substring(nearestAlphabetIndex, end_index+1),
        start: start_index + nearestAlphabetIndex
    }
}





/* find match positions given target string to match */
export function findTarget(target, locationContext) {   // given target string get char index of match
    textInPlay = getTextInPlay(locationContext)
    // return textInPlay.toLowerCase().lastIndexOf(target.toLowerCase())
    var re = new RegExp('(.+|^)\\b' + target + '\\b', 'gi');
    var match = re.exec(textInPlay); 
    if ( !match )
        return -1;
    return match[0].length - target.length
}

export function findTargetFromBoundaryString(target, locationContext, terminal_ind) {    // given begin/end target string, get start char index of the sentence if there's a match
    textInPlay = getTextInPlay(locationContext)
    var re; 

    if (terminal_ind.abs_target_loc === 'beginning')
        re = new RegExp('(.+\\.\\s*|^)('+target+')', 'gi')
    else if (terminal_ind.abs_target_loc === 'end')
        re = new RegExp('(.+\\.\\s+|^)(.+'+target+'\\s*\\.)', 'gi')
    
    var match = re.exec(textInPlay)
    if (!match)
        return -1

    console.log(`sentence beginning/ending with "${target}" at ${re.lastIndex - match[2].length}`)
    return re.lastIndex - match[2].length
}






/* Get Contexts */

export function getLeftContext(query, locationContext) {   /* get left context */
    textInPlay = getTextInPlay(locationContext)
    return overlap.left(textInPlay, query)
}

export function getRightContext(query, locationContext) {   /* get right context */
    textInPlay = getTextInPlay(locationContext)
    return overlap.right(textInPlay, query)
}



/* Quill Updates */

export function updateInsert(pos, slot, formatString) {
    quill.focus()
    return new Promise(function(resolve) {
        switch(formatString) {
            case 'newline': 
                quill.updateContents(new Delta()
                    .retain(pos)
                    .insert(slot, {'color': 'yellow'})
                    .retain(quill.getSelection().index))
                break;
            
            case 'insertAtBeginning':
                var alteredFirstChar = quill.getText(pos, 1).toLowerCase()
                var formatFirstChar = quill.getFormat(pos, 1)

                quill.updateContents(new Delta()
                    .retain(pos)
                    .insert(slot, {'color': 'yellow'})
                    .insert(' ')
                    .insert(alteredFirstChar, formatFirstChar)
                    .delete(1)
                    .retain(quill.getSelection().index));
                break;

            default:
                quill.updateContents(new Delta()
                    .retain(pos)
                    .insert(slot, {'color': 'yellow'})
                    .insert(' ')
                    .retain(quill.getSelection().index));
        }

        Promise.all([updateCompleted()]).then(function() {
            resolve();
        })
    })
    
    // typeof callback === 'function' && callback(); // calls the callback only if it's a func.
}

export function updateRemove(pos, nChar) {
    quill.focus()
    return new Promise(function(resolve) {
        quill.updateContents(new Delta()
            .retain(pos)
            .delete(nChar)
            .retain(quill.getSelection().index));

        Promise.all([updateCompleted()]).then(function() {
            resolve();
        })
    })
}

export function updateReplace(pos, nChar, slot, formatString) {
    quill.focus()
    return new Promise(function(resolve) {
        switch(formatString) {
            case 'noupdate':
                updateCompleted();
                break; 

            case 'changeline':
                quill.updateContents(new Delta()
                    .retain(pos)
                    .delete(nChar)
                    .insert(slot.replace(slot[0], slot[0].toUpperCase()) + '. ', {'color': 'orange'})
                    .retain(quill.getSelection().index));
                break;

            case 'endOfSentence':
                quill.updateContents(new Delta()
                    .retain(pos)
                    .delete(nChar)
                    .insert(slot.trim(), {'color': 'orange'})
                    .retain(quill.getSelection().index));
                break;

            default:
                quill.updateContents(new Delta()
                    .retain(pos)
                    .delete(nChar)
                    .insert(slot.trim() + ' ', {'color': 'orange'})
                    .retain(quill.getSelection().index));
        }

        Promise.all([updateCompleted()]).then(function() {
            resolve();
        })
    })
}

export function undoQuill() {
    quill.enable();
    return new Promise(function(resolve) {
        // console.log(quill.history)

        if (quill.history.stack.undo.length == 1) {
            read('There is nothing more to undo', 3)
            resolve()
        } else {

            quill.history.undo();

            Promise.all([updateCompleted()]).then(function() {
                quill.disable();
                resolve();
            })
        }
    })
}

export function redoQuill() {
    quill.enable();
    return new Promise(function(resolve) {
        // console.log(quill.history)

        if (quill.history.stack.redo.length == 0) {
            read('There is nothing more to redo', 3)
            resolve()
        } else {

            quill.history.redo();

            Promise.all([updateCompleted()]).then(function() {
                quill.disable();
                resolve();
            })
        }
    })
}

export function getSentenceAtLocationContext(locationContext) {
    console.log('Location Context (sent. index) :: ', locationContext);
    if ( gettext() == "" )
        return undefined
    else {
        var delimiterPosList = getDelimiterPositionsList();
        if (delimiterPosList.length == 0)
            return gettext() + '.';
        else if ( !isValid(locationContext) || locationContext <= 0 )
            return quill.getText(0, delimiterPosList[0] +1)    // return just the first sentence
        else {
            var ubound = delimiterPosList[locationContext]; // char index 
            var lbound = delimiterPosList[locationContext - 1] + 2 || 0
            return quill.getText(lbound, ubound - lbound +1)

        }
    }
}


