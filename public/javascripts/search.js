/* return: array value which is immediately greater than a 'value' taken as a param */

export default function(array, value) {
    var i;
    for (i = 0; i < array.length; i++) { 
        if (array[i] >= value)
            return i
    }
    return -1;
}