import getScore from './LM.js'
import {quill} from './quill.js'
import {ForwardNeg, ForwardPos, BackwardNeg, BackwardPos} from './traversal.js'
import execute from './execute.js'

/* working variables */
var queryText, r_context, lc_start; 
var quillText

const init = function(text, rc, start) {
    queryText = text; 
    r_context = rc;
    lc_start = start;
    quill.focus();
    quillText = quill.getText()
}

const preprocess = function(data, index) {
    var targetTokens = data.slice(0, index)
    var queryTokens = data.slice(index+1) 

    /* adjust headIndices for query */
    for ( var i = 0; i < queryTokens.length; i++ ) 
        queryTokens[i].headIndex = queryTokens[i].headIndex - ( index + 1 )

    return [targetTokens, queryTokens]
}

export default async function align(data, queryText, r_context, lc_start) { 
    init(queryText, r_context, lc_start)
    console.log('lc_start ::: ', lc_start)
    var index = data.findIndex(x => x.token === '.')

    data = preprocess(data, index);   // data is a single array of objects, should be split into target and query arrays and adjust head token indices for query
    var target = data[0],   // array of {token, tag, headIndex, label} for target
        query = data[1];    // array of {token, tag, headIndex, label} for query

    console.log('target Array', target)
    console.log('query Array', query)

    var r_context_wordCount = r_context.content.split(/\b\s+/).length
    var t_start = target.length - r_context_wordCount,
        q_start = query.length  - r_context_wordCount

    console.log('target start index', t_start)
    console.log('query start index', q_start)
    
    // check for the most common and obvious replacement - one to one alignment
    var save_t_start = t_start,
        save_q_start = q_start; 


    while(target[--t_start].tag == query[--q_start].tag);
    if(!query[q_start]) {
        return new Promise(function(resolve) {
            repair(target, t_start).then(function(response) {
                execute(response)
            })
        })
    }
    else {
        t_start = save_t_start; q_start = save_q_start;
    }



    // check for insertion of query just before the right context
    var tokenString; 
    if (t_start == 0)
        tokenString = query[q_start -1].token + '+' + query[q_start].token 
    else        
        tokenString = target[t_start -2].token + '+' + target[t_start-1].token + '+' + query[0].token +  '+' + query[1].token

    var insertScoreObj = await getScore(tokenString)
    var insertScore
    // console.log('value of score inserting query after', target[t_start-1].token, "=", insertScoreObj)
    try {
        insertScore =  JSON.parse(insertScoreObj).phrases[0].mc
    } catch(TypeError) {
        insertScore = 0
    }
    if (insertScore > 2000) {
        return new Promise(function(resolve) {
            repair(target, t_start -1).then(function(response) {
                execute(response)
            })
        })
    }

    var targetFNPath = ForwardNeg(target), // Forward Negative Path: <Forward for tail to head directed arrow> <Neg for direction of R to L traversal>
        queryFNPath  = ForwardNeg(query);

    console.log('target Forward Negative Path', targetFNPath)
    console.log('query Forward Negative Path', queryFNPath)
    
    var targetBNPath = BackwardNeg(target),
        queryBNPath  = BackwardNeg(query);
    
    console.log('target Backward Negative Path', targetBNPath)
    console.log('query Backward Negative Path', queryBNPath)

    var chkTarget = () => {
        console.log('checking for Target')
        return new Promise(function(resolve) {
            if ( !targetFNPath[t_start] && !targetBNPath[t_start] ) {
            /* if there's no FN and BN path then need to shift base, so follow BP path till a node with FN path is encountered */
                var targetBPPath = BackwardPos(target);
                console.log('shifting base on BP Path')
                var start_ = shiftBase(targetBPPath, t_start, targetFNPath)
                console.log('start_', start_)
                if (start_) 
                    resolve(start_)
                else {
                    console.log('shifting base on FP Path')
                    var targetFPPath = ForwardPos(target);
                    start_ = shiftBase(targetFPPath, t_start, targetFNPath)
                    console.log('start_', start_)
                    if (start_) 
                        resolve(start_)
                }
            }
            else
                resolve(t_start)
        })
    } 

    var chkQuery = () => {
        console.log('checking for Query')
        return new Promise(function(resolve) {
            if ( !queryFNPath[q_start] && !queryBNPath[q_start] ) {
            /* if there's no FN and BN path then need to shift base, so follow BP path till a node with FN path is encountered */
                var queryBPPath = BackwardPos(query);
                console.log('shifting base on BP Path')
                var start_ = shiftBase(queryBPPath, q_start, queryFNPath)
                console.log('start_', start_)
                if (start_)
                    resolve(start_)
                else {
                    console.log('shifting base on FP Path')
                    var queryFPPath = ForwardPos(query);
                    start_ = shiftBase(queryFPPath, q_start, queryFNPath)
                    console.log('start_', start_)
                    if (start_) 
                        resolve(start_)
                }
            }
            else
                resolve(q_start)
        })
    }
    
    t_start = await chkTarget()     // t_start and q_start are integers and not objects 
    q_start = await chkQuery()

    console.log('t_start and q_start after check :: ', t_start, q_start)

    // var targetFPPath = ForwardPos(target),
    //     queryFPPath  = ForwardPos(query)    

    // console.log('target Forward Positive Path', targetFPPath)
    // console.log('query Forward Positive Path', queryFPPath)
    
    var t_nextTokenIndex = targetFNPath[t_start], // nextTokenIndex is an object {index: , blocked: }
        q_nextTokenIndex = queryFNPath[q_start]

    /* do a forward negative pass to location the region of insertion */    
    if (t_nextTokenIndex && q_nextTokenIndex)
        while ( target[t_nextTokenIndex.index].tag == query[q_nextTokenIndex.index].tag) {
            console.log('target token', target[t_nextTokenIndex.index].token)
            console.log('target tag', target[t_nextTokenIndex.index].tag)
            console.log('query token', query[q_nextTokenIndex.index].token)
            console.log('query tag', query[q_nextTokenIndex.index].tag)

            if ( targetFNPath[t_nextTokenIndex.index] && queryFNPath[q_nextTokenIndex.index] ) {
                t_nextTokenIndex = targetFNPath[t_nextTokenIndex.index]
                q_nextTokenIndex = queryFNPath[q_nextTokenIndex.index]
            }
            else    
                break;
        }
    else {
        t_nextTokenIndex = {index: t_start, blocked: false}
        q_nextTokenIndex = {index: q_start, blocked: false}
    }

    console.log('after back pass tokenIndex', t_nextTokenIndex, q_nextTokenIndex)

    // extract the full phrase on which the last matched token from the FN path (to locate the region in the primary FN pass) depended
    // BN and FP can have multiple values for one key
    // extract the modifying (regulating) phrase 
    // doing only BN path for now — because the junction is the BN junction , FP would just provide additional information

    var queryPhraseArray = await getRegulatingPhrase(queryBNPath, q_nextTokenIndex);
    var targetPhraseArray = await getRegulatingPhrase(targetBNPath, t_nextTokenIndex);

    console.log('queryPhraseArray', queryPhraseArray)
    console.log('targetPhraseArray', targetPhraseArray)

    // var partition;
    // for (var i = 0; i < targetPhraseArray.length; i++)
    //     if (targetPhraseArray[i].label == 'CONJ') {
    //         partition = targetPhraseArray.slice(0)
    //         break;
    //     }
    
    // if (!partition)

    var partition = partitionArrayBySimilarity(target, query, targetPhraseArray, queryPhraseArray)
    console.log('partition of target array', partition)

    // now check first indices of partition and query to check for valid replacement patterns
    var replacementValidity = isValid(target[partition[0]], query[0]);
    console.log('replacement validity', replacementValidity)

    var repairIndex; 
    if (replacementValidity == true) {
        repairIndex = targetPhraseArray[0] - 1;
    }
    else {
        var score = await getLMScores(target, query, partition);
        
        /* sort score object on language score */ 
        var scoreSorted = Object.keys(score).sort(function(a,b){return score[b]-score[a]})
        console.log('sorted score object', scoreSorted)
        // repairIndex = parseInt(Object.keys(scoreSorted)[0], 10); // base 10 (decimal number sys.)
        repairIndex = scoreSorted[0]
        console.log('repairIndex', repairIndex)

        if (score[repairIndex] < 500)
            repairIndex--; 
    }

    return new Promise(function(resolve) {
        repair(target, repairIndex).then(function(response) {
            execute(response)
        })
    })
}

function getLMScores(target, query, partition) {
    console.log('inside getLMScores(target, query, partition)')
    return new Promise(async function(resolve) {
        var score = {}, tokenString; 

        for(const index of partition) {
            /* algo: 1
            if(index == 0) 
                tokenString = target[index].token + '+' + query[0].token + '+' + query[1].token;
            else
                tokenString = target[index-1].token + '+' + target[index].token + '+' + query[0].token + '+' + query[1].token;
            */
            /* algo: 2 */
            tokenString = target[index].token + '+' + query[0].token + '+' + query[1].token;
            console.log('tokenstring :: ', tokenString)

            var lmScore = await getScore(tokenString)
            console.log('value of score inserting query after', target[index].token, "=", lmScore)
            try {
                score[index] = JSON.parse(lmScore).phrases[0].mc
            } catch(TypeError) {
                score[index] = 0;
            }
        }
        
        console.log('score object', score)
        resolve(score)
    });
}

/* repair function */
function repair(target, repairIndex) { // query will be inserted after repairIndex
    return new Promise(function(resolve, reject) {
        var lc_length = 0;
        for( var i = 0; i <= repairIndex; i++)
            lc_length += target[i].token.length + ' '.length;
        
        --lc_length;
        if (lc_length < 0)
            lc_length = 0;
        
        var l_context_content = quillText.substring(lc_start, lc_start + lc_length);

        console.log('left content', l_context_content);
        
        var contextObj = {
            left: {
                content: l_context_content,
                location: lc_start
            },

            right: {
                content: r_context.content,
                location: r_context.location
            }
        }

        console.log('contextObj', contextObj)

        var operation = {
            intent: 'replace_L∂R',
            context: contextObj,
            slot: contextObj.left.content + ' ' + queryText.trim()
        }

        console.log(operation)

        if (operation)
            resolve(operation)
    })
}

function getRegulatingPhrase(BNPath, indexObj) {
    return new Promise(function(resolve, reject) {
        // var phraseArray = traverse(BNPath, FPPath, i)
        var phraseArray = traverse(BNPath, indexObj, [])
        if(phraseArray)
            resolve(phraseArray)
    });
}

function traverse(BNPath, node, phraseArray) {   // node is an object eg. {index:0, blocked: false}
    console.log('node', node)
    if (node === undefined)
        return

    console.log('BN['+node.index+']', BNPath[node.index]) 
    if (BNPath[node.index]) 
        BNPath[node.index].forEach(element => {
            console.log('element :: ', element)
            traverse(BNPath, element, phraseArray)
            phraseArray.push(element.index)
        });

    // phraseArray.push(node.index);

    // if (FPPath[node.index])
    //     FPPath[node.index].forEach(element => {
    //         console.log('element :: ', element)
    //         phraseArray.push(element.index)
    //         traverse(FPPath, element, phraseArray)
    //     });

    console.log(phraseArray)
    return phraseArray
}

function isValid(token1, token2) {
    console.log('checking validity of possible replacement')

    console.log('token1.tag', token1.tag)
    console.log('token1.label', token1.label)
    console.log('token2.tag', token2.tag)
    console.log('token2.label', token2.label)

    if (token1.tag == 'PRON' && token1.label == 'POSS') {
        if ( (token2.tag == 'PRON' && token2.label == 'POSS') || (token2.tag == 'NOUN' && token2.proper == 'PROPER') 
            || (token2.tag == 'PRON' && token2.label != 'POSS') )
            return true
    }
    else if (token1.tag == 'PRON' && token1.label != 'POSS') 
        return true
    else if (token1.tag == 'NOUN' && token1.label == 'NSUBJ') {
        if ( (token2.tag == 'NOUN' && token2.label == 'NSUBJ') || (token2.tag == 'PRON') )
            return true
    }
    return false;
}

function similar(token1, token2) {
    if ( (token1.tag == token2.tag) )
        return true
    if ( (token1.tag == 'PRON' && token2.tag == "NOUN") || (token1.tag == 'NOUN' && token2.tag == "PRON") )
        return true;
    if ( (token1.tag == 'VERB' && token2.tag == "ADV") || (token1.tag == 'ADV' && token2.tag == "VERB") )
        return true;

    return false; 
}   

function partitionArrayBySimilarity(target, query, t_arr, q_arr) {
    /* extract the similar partition */
    var partitionArray = new Array();  

    let ccIndex = target.findIndex(token => token.label == 'CC');
    if ( t_arr.indexOf(ccIndex) != -1 ) {
        for (var j_ = 0; j_ < q_arr.length; j_++) {
            for (var i_ = t_arr.length -1; i_ >= 0; i_--) {
                console.log(target[t_arr[i_]], query[q_arr[j_]] )
                if ( target[t_arr[i_]].tag == query[q_arr[j_]].tag )
                    return [t_arr[i_]]
            }
        }
    }
        
    var i = t_arr.length -1, 
        j = q_arr.length -1;

    for (; i >= 0 && j >=0; i--, j--) {
        if ( (target[i].token === query[j].token) && (target[i].tag === query[j].tag) && (target[i].label === query[j].label) )
            continue;

        else {
            partitionArray.push(t_arr[i])
            if ( !similar(target[i], query[j]) ) {
                console.log(target[i], 'and', query[j], 'are not similar')
                break;
            }
        }
    }
    // console.log('after partitionArray loop, i :: ', i, ' and j :: ', j)

    if( i >= 0 && j < 0 ) 
        partitionArray.push(t_arr[i])
        
    return partitionArray
}

function shiftBase(path, start, FNPath) {   // returns next which should be an integer
    console.log('inside shiftBase')
    console.log('path :: ', path)
    console.log('start :: ', start)
    console.log('FNPath :: ', FNPath)

    var next = path[start]
    while ( next ) {
        if ( FNPath[next.index] )
            break;

        if ( path[next.index] )
            next = path[next.index]
        else    
            break;
    }
    console.log('next', next)

    var node = FNPath[next.index]
    console.log ('node after shiftBase loop', node)
    if ( node && node.index < start ) // path found 
        return next.index
    return undefined
}
